import asyncio
from asyncio import sleep
from datetime import datetime
from unittest.mock import MagicMock

import pytest
from apscheduler.triggers.date import DateTrigger
from asgi_lifespan import LifespanManager
from fastapi import Depends
from httpx import AsyncClient

from fastapi_scheduler import FastScheduler


app = FastScheduler()

mock = MagicMock()


@pytest.fixture(scope='session')
def event_loop():
    return asyncio.new_event_loop()


def get_user():
    return 'test_user'


@app.job(trigger=DateTrigger(run_date=datetime.now()), timeout=1)
async def my_job(user: str = Depends(get_user)):
    mock(user=user)
    return user


@pytest.fixture(scope='session')
async def scheduler_app_client(event_loop) -> AsyncClient:
    async with AsyncClient(app=app, base_url='http://test') as client, LifespanManager(app):
        yield client


async def test_scheduler(scheduler_app_client):
    await sleep(0.5)
    mock.assert_called_once_with(user='test_user')
