from typing import Callable

import async_timeout
from apscheduler.executors.asyncio import AsyncIOExecutor
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger
from fastapi import FastAPI
from fastapi_utils.depends import run_function_with_depends


class FastScheduler(FastAPI):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.scheduler = AsyncIOScheduler(
            executors={
                'default': AsyncIOExecutor(),
            },
            job_defaults={
                'coalesce': True,
                'max_instances': 1,
            },
        )

        app = self

        @app.on_event('startup')
        async def startup():
            app.scheduler.start()

        @app.on_event('shutdown')
        async def shutdown():
            app.scheduler.shutdown()

    def job(self, trigger: CronTrigger | IntervalTrigger | DateTrigger, timeout: int | float):
        app = self

        def wrapper(func: Callable):
            app.scheduler.add_job(
                app._execute_job,
                trigger,
                kwargs={
                    'func': func,
                    'timeout': timeout,
                },
            )
            return func

        return wrapper

    async def _execute_job(self, func: Callable, timeout: int | float):
        async with async_timeout.timeout(timeout):
            result = await run_function_with_depends(
                app=self,
                func=func,
            )
            return result
